package de.ls5.wt2.entity;

public enum Role {
    NONE,
    USER,
    ADMIN
}
