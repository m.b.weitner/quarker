package de.ls5.wt2.entity;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class DBChat extends DBIdentified {

    // associates chat messages to 2 users

    // id is auto-injected
    private DBUser user1;
    private DBUser user2;

    @OneToOne
    public DBUser getUser1() {
        return user1;
    }

    public void setUser1(DBUser user1) {
        this.user1 = user1;
    }

    @OneToOne
    public DBUser getUser2() {
        return user2;
    }

    public void setUser2(DBUser user2) {
        this.user2 = user2;
    }
}
