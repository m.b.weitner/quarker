package de.ls5.wt2.entity;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import javax.persistence.*;
import java.util.Date;

@Entity
public class DBQuassel extends DBIdentified {

    // represents private message from one user to a second one
    private DBChat chat;
    private DBUser sender;
    private DBUser receiver;
    private Date dateSent;
    private boolean edited;
    private boolean read;
    private String message;

    @OneToOne
    public DBChat getChat() {
        return chat;
    }

    public void setChat(DBChat chatId) {
        this.chat = chatId;
    }

    @OneToOne
    public DBUser getSender() {
        return sender;
    }

    public void setSender(DBUser sender) {
        this.sender = sender;
    }

    @OneToOne
    public DBUser getReceiver() {
        return receiver;
    }

    public void setReceiver(DBUser receiver) {
        this.receiver = receiver;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(iso = ISO.DATE_TIME)
    public Date getDateSent() {
        return dateSent;
    }

    public void setDateSent(Date dateSend) {
        this.dateSent = dateSend;
    }

    public boolean isEdited() {
        return edited;
    }

    public void setEdited(boolean edited) {
        this.edited = edited;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
