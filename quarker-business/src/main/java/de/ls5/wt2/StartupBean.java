package de.ls5.wt2;

import java.util.ArrayList;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import de.ls5.wt2.entity.DBChat;
import de.ls5.wt2.entity.DBQuark;
import de.ls5.wt2.entity.DBUser;
import de.ls5.wt2.factory.ChatFactory;
import de.ls5.wt2.factory.QuarkFactory;
import de.ls5.wt2.factory.QuasselFactory;
import de.ls5.wt2.factory.UserFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
@Transactional
public class StartupBean implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private EntityManager entityManager;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        //how many random User
        final int userCount = 500;
        //how many quarks to maximal create by startup
        final int maxQuarkCount = 10;
        //how many chats per created user to maximal create by startup
        final int maxChatCount = 5;
        //how many quassel per Chat to maximal create by startup
        final int maxQuasselCount = 250;

        final DBQuark firstQuark = this.entityManager.find(DBQuark.class, 1L);

        // only initialize once
        if (firstQuark == null) {
            DBUser user = UserFactory.createDaniel(this.entityManager);
            DBUser user2 = UserFactory.createMaurice(this.entityManager);
            //creates some DBUser for the Database
            ArrayList<DBUser> userList = UserFactory.createRandomDummyUsers(this.entityManager, userCount);
            //create some Quarks for DBUser
            for (DBUser u : userList) {
                QuarkFactory.createRandomQuarks(entityManager, u, maxQuarkCount);
            }
            //create some DBChat between Users
            ArrayList<DBChat> chatList = ChatFactory.createChats(entityManager,userList,maxChatCount);

            chatList.forEach(c -> QuasselFactory.createQuassels(entityManager,c,maxQuasselCount));
        }
    }
}
