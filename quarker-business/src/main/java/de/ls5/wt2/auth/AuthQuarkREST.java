package de.ls5.wt2.auth;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Root;

import de.ls5.wt2.conf.auth.permission.EditQuarkPermission;
import de.ls5.wt2.conf.auth.permission.ViewFirstFiveQuarksPermission;
import de.ls5.wt2.entity.DBQuark;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@Transactional
@RestController
@RequestMapping(path = {"rest/auth/session/quark", "rest/auth/basic/quark", "rest/auth/jwt/quark"})
public class AuthQuarkREST {

    @Autowired
    private EntityManager entityManager;

    /**
     * @return all existing Quarks in database
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DBQuark>> readAllAsJSON() {
        final Subject subject = SecurityUtils.getSubject();
        if (subject == null || !subject.isAuthenticated()) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<DBQuark> query = builder.createQuery(DBQuark.class);
        final Root<DBQuark> from = query.from(DBQuark.class);

        query.select(from);

        final List<DBQuark> result = this.entityManager.createQuery(query).getResultList();
        return ResponseEntity.ok(result);
    }

    // role checking for role permission
    // SecurityUtils.getSubject().checkRole("admin");

    /**
     * @param param a new Quark
     * @return the created Quark
     */
    @PostMapping(path = "new",
                 consumes = MediaType.APPLICATION_JSON_VALUE,
                 produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DBQuark> create(@RequestBody final DBQuark param) {
        final Subject subject = SecurityUtils.getSubject();
        if (subject == null || !subject.isAuthenticated()) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        final DBQuark quark = new DBQuark();

        quark.setHeadline(param.getHeadline());
        quark.setContent(param.getContent());
        quark.setPublishedOn(new Date());
        quark.setUser(param.getUser());
        quark.setEdited(false);

        this.entityManager.persist(quark);

        return ResponseEntity.ok(quark);
    }

    /**
     * @param param - updated Quark
     * @return the updated Quark. If Quark does not exist with {@code id} or {@code id} is missing then {@code null} is returned.
     */
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
                produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DBQuark> updateQuark(@RequestParam final Map<String, String> requestParams,
                               @RequestBody final DBQuark param) {
        // check for permission
        final Subject subject = SecurityUtils.getSubject();
        if (subject == null || !subject.isAuthenticated()) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        if (!requestParams.containsKey("id")) {
            return null;
        }

        // get quarkId
        final long id = Long.parseLong(requestParams.get("id"));

        DBQuark updatedQuark = new DBQuark();
        updatedQuark.setId(param.getId());
        updatedQuark.setPublishedOn(param.getPublishedOn());
        updatedQuark.setHeadline(param.getHeadline());
        updatedQuark.setContent(param.getContent());
        updatedQuark.setUser(param.getUser());
        updatedQuark.setEdited(true);

        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<DBQuark> query = builder.createQuery(DBQuark.class);
        final Root<DBQuark> from = query.from(DBQuark.class);

        query.select(from).where(builder.equal(from.get("id"), id));

        DBQuark oldQuark;
        try {
            oldQuark = this.entityManager.createQuery(query).getSingleResult();
        } catch (NoResultException ignored) {
            return null;
        }

        final Permission editPermission = new EditQuarkPermission(oldQuark);
        if (!subject.isPermitted(editPermission)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        boolean edited = false;

        // check what has changed
        if (updatedQuark.getContent() != null && !updatedQuark.getContent().equals(oldQuark.getContent())) {
            oldQuark.setContent(updatedQuark.getContent());
            edited = true;
        }

        if (updatedQuark.getHeadline() != null && !updatedQuark.getHeadline().equals(oldQuark.getHeadline())) {
            oldQuark.setHeadline(updatedQuark.getHeadline());
            edited = true;
        }

        if (edited) {
            oldQuark.setEdited(true);
        }

        this.entityManager.persist(oldQuark);
        return ResponseEntity.ok(oldQuark);
    }








    @GetMapping(path = "newest",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DBQuark>> readNewestQuarks() {
        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<DBQuark> query = builder.createQuery(DBQuark.class);

        final Root<DBQuark> from = query.from(DBQuark.class);

        final Order order = builder.desc(from.get("publishedOn"));

        query.select(from).orderBy(order);

        final List<DBQuark> result = this.entityManager.createQuery(query).getResultList();

        // Attribute based permission check using permissions
        final Subject subject = SecurityUtils.getSubject();
        final Permission firstFiveNewsItemsPermission = new ViewFirstFiveQuarksPermission(result);

        if (!subject.isPermitted(firstFiveNewsItemsPermission)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        return ResponseEntity.ok(result.subList(0, 5));
    }
}
