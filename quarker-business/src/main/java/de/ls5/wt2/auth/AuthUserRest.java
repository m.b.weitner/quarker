package de.ls5.wt2.auth;

import de.ls5.wt2.entity.DBUser;
import de.ls5.wt2.entity.Role;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
@RestController
@RequestMapping(path = {"rest/auth/session/user", "rest/auth/basic/user", "rest/auth/jwt/user"})
public class AuthUserRest {

    @Autowired
    private EntityManager entityManager;

    @GetMapping(path = "{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public DBUser getUserById(@PathVariable("id") final long id) {
        // any is allowed to request users
        return this.entityManager.find(DBUser.class, id);
    }

    @PostMapping(path = "register",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DBUser> create(@RequestBody final DBUser param) {
        final Subject subject = SecurityUtils.getSubject();
        if (subject != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final DBUser user = new DBUser();

        user.setFirstName(param.getFirstName());
        user.setLastName(param.getLastName());
        user.setUsername(param.getUsername());
        user.setPassword(param.getPassword());
        user.setRole(Role.USER);
        user.setSessionKey(param.getSessionKey());

        Map<String, String> map = new HashMap<>();
        map.put("username", user.getUsername());
        if (getUserByName(map) != null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        this.entityManager.persist(user);
        this.entityManager.flush();
        return ResponseEntity.ok(user);
    }

    /**
     * @param param {@code username}, {@code password}
     * @return the logged in user. If no username or password were given as well
     *         as the username does not in the database {@code null} is returned.
     */
    @PostMapping(path = "login",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DBUser> login(@RequestBody final DBUser param) {
        // no username and password given
        if (param.getUsername() == null || param.getPassword() == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        final String username = param.getUsername();
        final String password = param.getPassword();

        // check if user exists
        Map<String, String> map = new HashMap<String, String>();
        map.put("username", username);
        DBUser user = getUserByName(map).getBody();
        if (user == null) {
            // username does not exist
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Subject currentUser = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        token.setRememberMe(false);
        try {
            currentUser.login(token);
        } catch (AuthenticationException ae) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        return ResponseEntity.ok(user);
    }

    /**
     * @param requestParams - username
     * @return the user if exists. Else {@code null} is returned.
     */
    @GetMapping(path = "findByName",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DBUser> getUserByName(@RequestParam final Map<String, String> requestParams) {
        if (!requestParams.containsKey("username")) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        final String name = requestParams.get("username");
        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<DBUser> query = builder.createQuery(DBUser.class);
        final Root<DBUser> from = query.from(DBUser.class);

        query.where(builder.equal(from.get("username"), name));

        DBUser result = null;
        try {
            result = this.entityManager.createQuery(query).getSingleResult();
        } catch (NoResultException nre) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(result);
    }

    /**
     * @return all existing Users in database
     */
    @RequiresRoles("admin")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<DBUser>> readAllAsJSON() {
        final Subject subject = SecurityUtils.getSubject();
        if (subject == null || !subject.isAuthenticated()) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        System.out.println("subject: " + subject.getPrincipal());
        System.out.println("hasRole:" + subject.hasRole("admin"));
        System.out.println("valueOf:" + Role.ADMIN);
        System.out.println(subject.isPermitted("admin"));

        if (subject.hasRole(String.valueOf(Role.ADMIN))) {
            final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
            final CriteriaQuery<DBUser> query = builder.createQuery(DBUser.class);

            final Root<DBUser> from = query.from(DBUser.class);

            query.select(from);

            return ResponseEntity.ok(this.entityManager.createQuery(query).getResultList());
        }
        System.out.println("Es was schiefgelaufen!!!");
        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }

    @PutMapping(path = "update",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DBUser> update(@RequestParam final Map<String, String> requestParams,
                         @RequestBody final DBUser param) {
        final DBUser requested = new DBUser();
        requested.setId(param.getId());     // TODO: Refactor to not use user-given id, use username instead
        requested.setFirstName(param.getFirstName());
        requested.setLastName(param.getLastName());
        requested.setUsername(param.getUsername());
        requested.setPassword(param.getPassword());
        requested.setRole(param.getRole());
        requested.setSessionKey(param.getSessionKey());
        DBUser user = this.entityManager.find(DBUser.class, param.getId());

        if (user != null) {
            // user wants a new username
            if (requested.getUsername() != null && !requested.getUsername().equals(user.getUsername())) {
                // check for uniqueness
                Map<String, String> map = new HashMap<>();
                map.put("username", user.getUsername());
                if (getUserByName(map) == null) {
                    // username available
                    user.setUsername(requested.getUsername());
                }
            }

            // user wants a new first name
            if (requested.getFirstName() != null && !requested.getFirstName().equals(user.getFirstName())) {
                user.setFirstName(requested.getFirstName());
            }

            // user wants a new last name
            if (requested.getLastName() != null && !requested.getLastName().equals(user.getLastName())) {
                user.setLastName(requested.getLastName());
            }

            // user wants a new password
            if (requested.getPassword() != null && !requested.getPassword().equals(user.getPassword())) {
                user.setPassword(requested.getPassword());
            }

            this.entityManager.persist(user);
            this.entityManager.flush();
            return ResponseEntity.ok(user);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    /**
     * @param requestParams - {@code id} of the user to delete
     * @return the deleted user object. If user does not exist, {@code null} is returned.
     */
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DBUser> deleteUser(@RequestParam final Map<String, String> requestParams) {
        if (!requestParams.containsKey("id")) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        final long id = Long.parseLong(requestParams.get("id"));

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<DBUser> query = builder.createQuery(DBUser.class);
        Root<DBUser> from = query.from(DBUser.class);

        query.select(from).where(builder.equal(from.get("id"), id));

        DBUser user;
        try {
            user = this.entityManager.createQuery(query).getSingleResult();
        } catch (Exception ignored) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        this.entityManager.remove(user);
        this.entityManager.flush();
        return ResponseEntity.ok(user);
    }
}

