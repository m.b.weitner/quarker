package de.ls5.wt2.factory;

import de.ls5.wt2.entity.DBUser;
import de.ls5.wt2.entity.Role;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Random;

/**
 * Factory to create some DBUsers
 */
public class UserFactory {
    /**
     * Creates an DBUser named like one of the authors
     * @param entityManager of the DB
     * @return author Daniel
     */
    public static DBUser createDaniel(EntityManager entityManager) {
        DBUser user = new DBUser();
        user.setUsername("0xSeb");
        user.setFirstName("Daniel");
        user.setLastName("Hollweg");
        user.setPassword("0815");
        user.setRole(Role.ADMIN);
        user.setSessionKey("012345678");
        entityManager.persist(user);
        return user;
    }

    /**
     * Creates an DBUser named like one of the authors
     * @param entityManager of the db
     * @return author Maurice
     */
    public static DBUser createMaurice(EntityManager entityManager){
        DBUser user = new DBUser();
        user.setUsername("Momo154");
        user.setFirstName("Maurice");
        user.setLastName("Weitner");
        user.setPassword("42");
        user.setRole(Role.USER);
        user.setSessionKey("6654asd654a");
        entityManager.persist(user);
        return user;
    }

    /**
     * Creates some DBUser-entities and persist it to the DB
     * @param entityManager of the DB
     * @param userCount how many DBUser to create
     * @return list of created DBUser
     */
    public static ArrayList<DBUser> createRandomDummyUsers(EntityManager entityManager, int userCount){
        Random random = new Random();
        ArrayList<DBUser> userList = new ArrayList<>();
        for(int i = 0; i< FactoryStrings.names.length; i++){
            DBUser user = createRandomDummyUser(entityManager);
            userList.add(user);
        }
        return userList;
    }

    /**
     * Create one random DBUser and persist it to the DB
     * @param entityManager of the DB
     * @return created DBUser
     */
    public static DBUser createRandomDummyUser(EntityManager entityManager){
        Random random = new Random();
        DBUser user = new DBUser();
        int randomInt = random.nextInt();
        String name = FactoryStrings.randomName();
        user.setUsername(name+randomInt);
        user.setFirstName(name);
        user.setLastName(FactoryStrings.randomLastName());
        user.setPassword("" + randomInt);
        user.setRole(Role.USER);
        user.setSessionKey(""+random.nextInt());
        entityManager.persist(user);
        return user;
    }
}
