package de.ls5.wt2.factory;

import de.ls5.wt2.entity.DBChat;
import de.ls5.wt2.entity.DBQuassel;
import de.ls5.wt2.entity.DBUser;

import javax.persistence.EntityManager;
import java.util.Random;

/**
 * Factory to create some DBQuassel
 */
public class QuasselFactory {
    /**
     * Creates some DBQuassel for the DB
     * @param entityManager of the DB
     * @param chat to create the DBQuassel for
     * @param maxQuasselCount maximal count of DBQuarks to create
     */
    public static void createQuassels(EntityManager entityManager, DBChat chat, int maxQuasselCount){
        int count = FactoryUtils.randomInt(maxQuasselCount);
        for (int i = 0; i < count; i++) {
            createQuassel(entityManager,chat);
        }
    }

    /**
     * Creates one Quassel
     * @param entityManager of the DB
     * @param chat to create Qussel for
     */
    public static void createQuassel(EntityManager entityManager, DBChat chat){
        DBUser user1 = chat.getUser1();
        DBUser user2 = chat.getUser2();
        Random random = new Random();
        DBQuassel quassel = new DBQuassel();
        quassel.setChat(chat);
        quassel.setDateSent(FactoryUtils.randomDate());
        if (random.nextBoolean()){
            quassel.setSender(user1);
            quassel.setReceiver(user2);
        } else {
            quassel.setSender(user2);
            quassel.setReceiver(user1);
        }
        quassel.setEdited(random.nextBoolean());
        quassel.setRead(true);
        quassel.setMessage(FactoryStrings.randomSentence());
        entityManager.persist(quassel);
    }
}
