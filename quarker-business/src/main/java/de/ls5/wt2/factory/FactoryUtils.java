package de.ls5.wt2.factory;

import java.sql.Array;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * Util class for create dummy-elements
 */
public class FactoryUtils {
    public static long globalDate = Timestamp.valueOf("2013-01-01 00:00:00").getTime();
    /**
     * Creates a random date
     * @return random date
     */
    public static Date randomDate(){
        long date = globalDate + Math.abs((long)(Math.random() * 100000000));
        globalDate = date;
        return new Date(date);
    }

    /**
     * Creates a random index out of a given list
     * @param list to get random index from
     * @return random index
     */
    public static int randomIndexFromList(List list){
        Random random = new Random();
        return random.nextInt(list.size());
    }

    public static int randomIndexFromStringArray(String[] array){
        Random random = new Random();
        return random.nextInt(array.length);
    }

    public static int randomInt(int max){
        Random random = new Random();
        return random.nextInt(max) +1;
    }
}
