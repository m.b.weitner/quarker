package de.ls5.wt2.factory;

import de.ls5.wt2.entity.DBQuark;
import de.ls5.wt2.entity.DBUser;

import javax.persistence.EntityManager;
import java.util.Random;

/**
 * Factory to create DBQuarks
 */
public class QuarkFactory {
    /**
     * Creates some DBQuark entities and persist them
     * @param entityManager of the db
     * @param user to create the quarks for
     * @param maxQuarkCount maximal count of DBQuarks to create
     */
    public static void createRandomQuarks(EntityManager entityManager, DBUser user, int maxQuarkCount){
        int count = FactoryUtils.randomInt(maxQuarkCount);
        for (int i = 0; i < count; i++) {
            createRandomQuark(entityManager,user);
        }
    }

    /**
     * Creates one random Quark for the given DBUser
     * @param entityManager of the db
     * @param user to create the DBQuarks for
     */
    public static void createRandomQuark(EntityManager entityManager, DBUser user){
        DBQuark quark = new DBQuark();
        Random random = new Random();
        String headline = FactoryStrings.sentences[random.nextInt(FactoryStrings.sentences.length)];
        String content = FactoryStrings.sentences[random.nextInt(FactoryStrings.sentences.length)];

        quark.setHeadline(headline);
        quark.setContent(content);
        quark.setUser(user);
        quark.setPublishedOn(FactoryUtils.randomDate());

        entityManager.persist(quark);
    }
}
