package de.ls5.wt2.factory;

import de.ls5.wt2.entity.DBChat;
import de.ls5.wt2.entity.DBUser;

import javax.persistence.EntityManager;
import java.util.ArrayList;

/**
 * Factory to create some chats
 */
public class ChatFactory {

    /**
     * Creates some DummyChats and persist it to the DB
     * @param entityManager of the DB
     * @param users List to create for
     * @param maxChatCount maximal count of chats per user
     * @return list of all created chats
     */
    public static ArrayList<DBChat> createChats(EntityManager entityManager, ArrayList<DBUser> users, int maxChatCount){
        ArrayList<DBChat> chats = new ArrayList<>();
        for (DBUser user: users) {

            int count = FactoryUtils.randomInt(maxChatCount);
            for (int i = 0; i < count; i++) {
                DBChat chat = new DBChat();
                chat.setUser1(user);
                chat.setUser2(users.get(FactoryUtils.randomIndexFromList(users)));
                entityManager.persist(chat);
                chats.add(chat);
            }
        }
        return chats;
    }
}
