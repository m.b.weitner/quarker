package de.ls5.wt2.rest;

import de.ls5.wt2.entity.*;
import de.ls5.wt2.entity.DBQuassel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Transactional
@RestController
@RequestMapping(path = "rest/quassel/chat")
public class QuasselREST {

    @Autowired
    private EntityManager entityManager;

    /**
     *API GET-Request to get all DBQuassel with the given chatId
     * @return all quassel from the given chat
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DBQuassel> getQuasselFromChat(@RequestParam final Map<String, String> requestParams) {
        if (!requestParams.containsKey("chatId")) {
            return null;
        }

        final long id = Long.parseLong(requestParams.get("chatId"));

        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        final CriteriaQuery<DBChat> chatQuerry = builder.createQuery(DBChat.class);
        final Root<DBChat> fromChat = chatQuerry.from(DBChat.class);

        final CriteriaQuery<DBQuassel> quasselQuery = builder.createQuery(DBQuassel.class);
        final Root<DBQuassel> fromQuassel = quasselQuery.from(DBQuassel.class);
        System.out.println("ID:" + id);
        chatQuerry.select(fromChat).where(builder.equal(fromChat.get("id"), id));
        final DBChat chat = this.entityManager.createQuery(chatQuerry).getSingleResult();

        if (chat == null) {
            // chat does not exist in database
            return new LinkedList<>();
        }

        quasselQuery.select(fromQuassel).where(builder.equal(fromQuassel.get("chat"), chat)).orderBy(builder.desc(fromQuassel.get("id")));

        return this.entityManager.createQuery(quasselQuery).getResultList();
    }

    /**
     *API GET-Request to get 20 newest quassel from given chat
     * @return 20 quassel from the given chat
     */
    @GetMapping(path = "newest",
                produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DBQuassel> get20NewestQuasselFromChat(@RequestParam final Map<String, String> requestParams) {
        if (!requestParams.containsKey("chatId")) {
            return null;
        }

        final long id = Long.parseLong(requestParams.get("chatId"));

        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        final CriteriaQuery<DBChat> chatQuerry = builder.createQuery(DBChat.class);
        final Root<DBChat> fromChat = chatQuerry.from(DBChat.class);

        final CriteriaQuery<DBQuassel> quasselQuery = builder.createQuery(DBQuassel.class);
        final Root<DBQuassel> fromQuassel = quasselQuery.from(DBQuassel.class);
        System.out.println("ID:" + id);
        chatQuerry.select(fromChat).where(builder.equal(fromChat.get("id"), id));
        final DBChat chat = this.entityManager.createQuery(chatQuerry).getSingleResult();

        if (chat == null) {
            // chat does not exist in database
            return new LinkedList<>();
        }

        quasselQuery.select(fromQuassel).where(builder.equal(fromQuassel.get("chat"), chat)).orderBy(builder.desc(fromQuassel.get("id")));

        return this.entityManager.createQuery(quasselQuery).setMaxResults(20).getResultList();
    }

    /**
     *API GET-Request to get 20 newest quassel from given chat
     * @return 20 quassel from the given chat
     */
    @GetMapping(path = "from",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DBQuassel> get20OlderQuasselFromChat(@RequestParam final Map<String, String> requestParams) {
        if (!requestParams.containsKey("chatId") || !requestParams.containsKey("quasselId")) {
            return null;
        }

        final long chatId = Long.parseLong(requestParams.get("chatId"));
        final long quasselId = Long.parseLong(requestParams.get("quasselId"));

        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        final CriteriaQuery<DBChat> chatQuery = builder.createQuery(DBChat.class);
        final Root<DBChat> fromChat = chatQuery.from(DBChat.class);

        final CriteriaQuery<DBQuassel> quasselQuery = builder.createQuery(DBQuassel.class);
        final Root<DBQuassel> fromQuassel = quasselQuery.from(DBQuassel.class);
        chatQuery.select(fromChat).where(builder.equal(fromChat.get("id"), chatId));
        final DBChat chat = this.entityManager.createQuery(chatQuery).getSingleResult();

        if (chat == null) {
            // chat does not exist in database
            return new LinkedList<>();
        }

        quasselQuery.select(fromQuassel)
                .where(builder.and(
                        builder.equal(fromQuassel.get("chat"), chat)),
                        builder.lessThan(fromQuassel.get("id"),quasselId))
                .orderBy(builder.desc(fromQuassel.get("id")));
        return this.entityManager.createQuery(quasselQuery).setMaxResults(20).getResultList();
    }

    @PostMapping(path = "new",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public DBQuassel create(@RequestBody final DBQuassel param) {
        final DBQuassel quassel = new DBQuassel();

        quassel.setMessage(param.getMessage());
        quassel.setReceiver(param.getReceiver());
        quassel.setSender(param.getSender());

        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<DBChat> chatQuery = builder.createQuery(DBChat.class);
        final Root<DBChat> fromChat = chatQuery.from(DBChat.class);
        chatQuery.select(fromChat).where(
                builder.and(
                        builder.and(
                                builder.equal(fromChat.get("id"),param.getChat().getId()),
                                builder.equal(fromChat.get("user1"), param.getChat().getUser1())),
                        builder.equal(fromChat.get("user2"),param.getChat().getUser2()))
        );
        DBChat chat = this.entityManager.createQuery(chatQuery).getSingleResult();
        if (chat == null){
            return null;
        }
        quassel.setChat(chat);
        this.entityManager.persist(quassel);

        return quassel;
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public DBQuassel updateQuassel(@RequestParam final Map<String, String> requestParams,
                               @RequestBody final DBQuassel param) {
        if (!requestParams.containsKey("quasselId") || Long.parseLong(requestParams.get("quasselId")) != param.getId()) {
            return null;
        }

        final long quasselId = Long.parseLong(requestParams.get("quasselId"));

        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<DBQuassel> query = builder.createQuery(DBQuassel.class);
        final Root<DBQuassel> from = query.from(DBQuassel.class);

        query.select(from).where(builder.equal(from.get("id"), quasselId));

        DBQuassel oldQuassel = null;
        try {
            oldQuassel = this.entityManager.createQuery(query).getSingleResult();
        } catch (Exception ignored) {
        }
        if (oldQuassel == null){
            return null;
        }
        oldQuassel.setEdited(true);
        oldQuassel.setMessage(param.getMessage());

        entityManager.persist(oldQuassel);
        return oldQuassel;
    }

    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public DBQuassel deleteQuassel(@RequestParam final Map<String, String> requestParams) {
        if (!requestParams.containsKey("quasselId")) {
            return null;
        }

        final long quasselId = Long.parseLong(requestParams.get("quasselId"));

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<DBQuassel> quasselQuery = builder.createQuery(DBQuassel.class);
        Root<DBQuassel> fromQuassel = quasselQuery.from(DBQuassel.class);

        quasselQuery.select(fromQuassel).where(builder.equal(fromQuassel.get("id"), quasselId));

        DBQuassel quassel = null;
        try {
            quassel = this.entityManager.createQuery(quasselQuery).getSingleResult();
        } catch (Exception ignored) {
        }

        this.entityManager.remove(quassel);
        this.entityManager.flush();
        return quassel;
    }
}
