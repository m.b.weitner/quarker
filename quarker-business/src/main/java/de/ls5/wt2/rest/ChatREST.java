package de.ls5.wt2.rest;

import de.ls5.wt2.entity.DBChat;
import de.ls5.wt2.entity.DBUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Map;

@Transactional
@RestController
@RequestMapping(path = "rest/chat")
public class ChatREST {
    @Autowired
    private EntityManager entityManager;

    @GetMapping(path="byId",
                produces = MediaType.APPLICATION_JSON_VALUE)
    public DBChat getChatById(@RequestParam final Map<String, String> requestParams) {
        final long id = Long.parseLong(requestParams.get("chatId"));
        return this.entityManager.find(DBChat.class, id);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DBChat> getAllChats() {
        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<DBChat> chatQuery = builder.createQuery(DBChat.class);
        final Root<DBChat> fromChat = chatQuery.from(DBChat.class);

        chatQuery.select(fromChat);

        return this.entityManager.createQuery(chatQuery).getResultList();

    }
}
