package de.ls5.wt2.rest;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;

import de.ls5.wt2.entity.DBQuark;
import de.ls5.wt2.entity.DBQuark;
import de.ls5.wt2.entity.DBUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@Transactional
@RestController
@RequestMapping(path = "rest/quark")
public class QuarkREST {

    @Autowired
    private EntityManager entityManager;

    /**
     * @return all existing Quarks in database
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DBQuark> readAllAsJSON() {
        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<DBQuark> query = builder.createQuery(DBQuark.class);

        final Root<DBQuark> from = query.from(DBQuark.class);

        query.select(from);

        return this.entityManager.createQuery(query).getResultList();
    }

    /**
     * @param param a new Quark
     * @return the created Quark
     */
    @PostMapping(path = "new",
                 consumes = MediaType.APPLICATION_JSON_VALUE,
                 produces = MediaType.APPLICATION_JSON_VALUE)
    public DBQuark create(@RequestBody final DBQuark param) {
        final DBQuark quark = new DBQuark();

        quark.setHeadline(param.getHeadline());
        quark.setContent(param.getContent());
        quark.setPublishedOn(new Date());
        quark.setUser(param.getUser());
        quark.setEdited(false);

        this.entityManager.persist(quark);

        return quark;
    }

    /**
     * @param param - updated Quark
     * @return the updated Quark. If Quark does not exist with {@code id} or {@code id} is missing then {@code null} is returned.
     */
    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
                produces = MediaType.APPLICATION_JSON_VALUE)
    public DBQuark updateQuark(@RequestParam final Map<String, String> requestParams,
                               @RequestBody final DBQuark param) {
        if (!requestParams.containsKey("id")) {
            return null;
        }

        final long id = Long.parseLong(requestParams.get("id"));

        DBQuark updatedQuark = new DBQuark();
        updatedQuark.setId(param.getId());
        updatedQuark.setPublishedOn(param.getPublishedOn());
        updatedQuark.setHeadline(param.getHeadline());
        updatedQuark.setContent(param.getContent());
        updatedQuark.setUser(param.getUser());
        updatedQuark.setEdited(true);

        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<DBQuark> query = builder.createQuery(DBQuark.class);
        final Root<DBQuark> from = query.from(DBQuark.class);

        query.select(from).where(builder.equal(from.get("id"), id));

        DBQuark oldQuark = null;
        try {
            oldQuark = this.entityManager.createQuery(query).getSingleResult();
        } catch (Exception ignored) {
        }

        if (oldQuark == null) {
            return null;
        }

        boolean edited = false;

        // check what has changed
        if (updatedQuark.getContent() != null && !updatedQuark.getContent().equals(oldQuark.getContent())) {
            oldQuark.setContent(updatedQuark.getContent());
            edited = true;
        }

        if (updatedQuark.getHeadline() != null && !updatedQuark.getHeadline().equals(oldQuark.getHeadline())) {
            oldQuark.setHeadline(updatedQuark.getHeadline());
            edited = true;
        }

        if (edited) {
            oldQuark.setEdited(true);
        }

        this.entityManager.persist(oldQuark);

        return oldQuark;
    }

    /**
     * @param requestParams - {@code id} of the requested Quark
     * @return the Quark with {@code id}. If {@code id} does not exist or param {@code id}
     *         is missing, {@code null} is returned.
     */
    @GetMapping(path = "byId",
                produces = MediaType.APPLICATION_JSON_VALUE)
    public DBQuark readAsJSON(@RequestParam final Map<String, String> requestParams) {
        if (!requestParams.containsKey("id")) {
            return null;
        }
        final long id = Long.parseLong(requestParams.get("id"));

        return this.entityManager.find(DBQuark.class, id);
    }

    /**
     * @return all Quarks from user with {@code username}. Else an
     *         empty list is returned if the user does not exist.
     */
    @GetMapping(path = "user",
                produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DBQuark> getQuarkByUserId(@RequestParam final Map<String, String> requestParams) {
        if (!requestParams.containsKey("id")) {
            return null;
        }

        final long id = Long.parseLong(requestParams.get("id"));

        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();

        final CriteriaQuery<DBQuark> query = builder.createQuery(DBQuark.class);
        final Root<DBQuark> from = query.from(DBQuark.class);

        final CriteriaQuery<DBUser> subQuery = builder.createQuery(DBUser.class);
        final Root<DBUser> subQueryFrom = subQuery.from(DBUser.class);

        subQuery.select(subQueryFrom).where(builder.equal(subQueryFrom.get("id"), id));
        final DBUser user = this.entityManager.createQuery(subQuery).getSingleResult();

        if (user == null) {
            // user does not exist in database
            return new LinkedList<>();
        }

        query.select(from).where(builder.equal(from.get("user"), user));

        return this.entityManager.createQuery(query).getResultList();
    }

    /**
     * @param requestParams - {@code id} of the first Quark
     * @return the next 20 Quarks starting with {@code id} as lowest id with increasing id
     */
    @GetMapping(path = "from",
                produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DBQuark> getQuarkFrom(@RequestParam final Map<String, String> requestParams) {
        if (!requestParams.containsKey("id")) {
            return null;
        }

        final long id = Long.parseLong(requestParams.get("id"));

        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<DBQuark> query = builder.createQuery(DBQuark.class);

        final Root<DBQuark> from = query.from(DBQuark.class);

        query.select(from).where(builder.ge(from.get("id"), id)).orderBy(builder.desc(from.get("id")));

        return this.entityManager.createQuery(query).setMaxResults(20).getResultList();
    }

    /**
     * @param requestParams parameter for user and quark
     * @return at most 20 Quark starting with {@code quarkId} from user with {@code userId}. If user does
     *         not exist, {@code null} is returned.
     */
    @GetMapping(path = "from/user",
                produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DBQuark> latestQuarksByUser(@RequestParam final Map<String, String> requestParams) {
        if (!requestParams.containsKey("userId") || !requestParams.containsKey("quarkId")) {
            return null;
        }

        final long userId = Long.parseLong(requestParams.get("userId"));
        final long quarkId = Long.parseLong(requestParams.get("quarkId"));

        // check if user exists
        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<DBUser> queryUser = builder.createQuery(DBUser.class);
        final Root<DBUser> fromUser = queryUser.from(DBUser.class);
        queryUser.select(fromUser).where(builder.equal(fromUser.get("id"), userId));

        DBUser user = null;
        try {
            user = this.entityManager.createQuery(queryUser).getSingleResult();
        } catch (Exception ignored) {
        }

        if (user == null) {
            return null;
        }

        final CriteriaQuery<DBQuark> queryQuark = builder.createQuery(DBQuark.class);
        final Root<DBQuark> fromQuark = queryQuark.from(DBQuark.class);
        queryQuark.select(fromQuark)
                    .where(builder.and(
                                    builder.equal(fromQuark.get("user"), user),
                                    builder.ge(fromQuark.get("id"), quarkId)))
                    .orderBy(builder.desc(fromQuark.get("id")));

        return this.entityManager.createQuery(queryQuark).setMaxResults(20).getResultList();
    }

    /**
     * @return the newest 5 Quarks
     */
    @GetMapping(path = "newest",
                produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DBQuark> readNewestQuark() {
        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<DBQuark> query = builder.createQuery(DBQuark.class);

        final Root<DBQuark> from = query.from(DBQuark.class);

        query.select(from).orderBy(builder.desc(from.get("publishedOn")));

        return this.entityManager.createQuery(query).setMaxResults(5).getResultList();
    }

    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public DBQuark deleteQuark(@RequestParam final Map<String, String> requestParams) {
        if (!requestParams.containsKey("quarkId")) {
            System.out.println("hallo");
            return null;
        }

        final long quarkId = Long.parseLong(requestParams.get("quarkId"));

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<DBQuark> quarkQuerry = builder.createQuery(DBQuark.class);
        Root<DBQuark> fromQuark = quarkQuerry.from(DBQuark.class);

        quarkQuerry.select(fromQuark).where(builder.equal(fromQuark.get("id"), quarkId));

        DBQuark quark = null;
        try {
            System.out.println("hallo2");
            quark = this.entityManager.createQuery(quarkQuerry).getSingleResult();
        } catch (Exception ignored) {
        }

        this.entityManager.remove(quark);
        this.entityManager.flush();
        return quark;
    }
}
