package de.ls5.wt2.rest;

import de.ls5.wt2.entity.DBUser;
import de.ls5.wt2.entity.Role;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.criteria.*;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Transactional
@RestController
@RequestMapping(path = "rest/user")
public class UserRest {

    @Autowired
    private EntityManager entityManager;

    @GetMapping(path = "{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public DBUser getUserById(@PathVariable("id") final long id) {
        System.out.println(id);
        return this.entityManager.find(DBUser.class, id);
    }

    @PostMapping(path = "register",
                 consumes = MediaType.APPLICATION_JSON_VALUE,
                 produces = MediaType.APPLICATION_JSON_VALUE)
    public DBUser create(@RequestBody final DBUser param) {
        final DBUser user = new DBUser();

        user.setFirstName(param.getFirstName());
        user.setLastName(param.getLastName());
        user.setUsername(param.getUsername());
        user.setPassword(param.getPassword());
        user.setRole(Role.USER);
        user.setSessionKey(param.getSessionKey());

        Map<String, String> map = new HashMap<>();
        map.put("username", user.getUsername());
        if (getUserByName(map) != null) {
            return null;
        }

        this.entityManager.persist(user);
        this.entityManager.flush();
        return user;
    }

    /**
     * @param param {@code username}, {@code password}
     * @return the logged in user. If no username or password were given as well
     *         as the username does not in the database {@code null} is returned.
     */
    @PostMapping(path = "login",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public DBUser login(@RequestBody final DBUser param) {
        // no username and password given
        if (param.getUsername() == null || param.getPassword() == null) {
            return null;
        }

        final String username = param.getUsername();
        final String password = param.getPassword();

        // check if user exists
        Map<String, String> map = new HashMap<String, String>();
        map.put("username", username);
        DBUser user = getUserByName(map);
        if (user == null) {
            // username does not exist
            return null;
        }

        Subject currentUser = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        token.setRememberMe(false);
        try {
            currentUser.login(token);
        } catch (AuthenticationException ae) {
            return null;
        }

        return user;
    }

    /**
     * @param requestParams - username
     * @return the user if exists. Else {@code null} is returned.
     */
    @GetMapping(path = "findByName",
                produces = MediaType.APPLICATION_JSON_VALUE)
    public DBUser getUserByName(@RequestParam final Map<String, String> requestParams) {
        if (!requestParams.containsKey("username")) {
            return null;
        }
        final String name = requestParams.get("username");
        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<DBUser> query = builder.createQuery(DBUser.class);
        final Root<DBUser> from = query.from(DBUser.class);

        query.where(builder.equal(from.get("username"), name));

        DBUser result = null;
        try {
            result = this.entityManager.createQuery(query).getSingleResult();
        } catch (NoResultException ignored) {
        }
        return result;
    }

    /**
     * @return all existing users
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<DBUser> readAllAsJSON() {
        final CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        final CriteriaQuery<DBUser> query = builder.createQuery(DBUser.class);

        final Root<DBUser> from = query.from(DBUser.class);

        query.select(from);

        return this.entityManager.createQuery(query).getResultList();
    }

    @PutMapping(path = "update",
                consumes = MediaType.APPLICATION_JSON_VALUE,
                produces = MediaType.APPLICATION_JSON_VALUE)
    public DBUser update(@RequestParam final Map<String, String> requestParams,
                         @RequestBody final DBUser param) {
        final DBUser requested = new DBUser();
        requested.setId(param.getId());     // TODO: Refactor to not use user-given id, use username instead
        requested.setFirstName(param.getFirstName());
        requested.setLastName(param.getLastName());
        requested.setUsername(param.getUsername());
        requested.setPassword(param.getPassword());
        requested.setRole(param.getRole());
        requested.setSessionKey(param.getSessionKey());
        DBUser user = this.entityManager.find(DBUser.class, param.getId());

        if (user != null) {
            // user exists
            System.out.println(!requested.getSessionKey().equals(requestParams.get("sessionKey")));
            System.out.println("requested.getSessionKey():" + requested.getSessionKey());
            System.out.println("requestParams.get(\"sessionKey\"):" + requestParams.get("sessionKey"));
            if (!requested.getSessionKey().equals(requestParams.get("sessionKey"))) {
                return null;
            }

            // TODO: admins können noch nicht die Daten von anderen Nutzern ändern

            // user wants a new username
            if (requested.getUsername() != null && !requested.getUsername().equals(user.getUsername())) {
                // check for uniqueness
                Map<String, String> map = new HashMap<>();
                map.put("username", user.getUsername());
                if (getUserByName(map) == null) {
                    // username available
                    user.setUsername(requested.getUsername());
                }
            }

            // user wants a new first name
            if (requested.getFirstName() != null && !requested.getFirstName().equals(user.getFirstName())) {
                user.setFirstName(requested.getFirstName());
            }

            // user wants a new last name
            if (requested.getLastName() != null && !requested.getLastName().equals(user.getLastName())) {
                user.setLastName(requested.getLastName());
            }

            // user wants a new password
            if (requested.getPassword() != null && !requested.getPassword().equals(user.getPassword())) {
                user.setPassword(requested.getPassword());
            }

            this.entityManager.persist(user);
            this.entityManager.flush();
        }
        return user;
    }

    /**
     * @param requestParams - {@code id} of the user to delete
     * @return the deleted user object. If user does not exist, {@code null} is returned.
     */
    @DeleteMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public DBUser deleteUser(@RequestParam final Map<String, String> requestParams) {
        if (!requestParams.containsKey("id")) {
            return null;
        }

        final long id = Long.parseLong(requestParams.get("id"));

        CriteriaBuilder builder = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<DBUser> query = builder.createQuery(DBUser.class);
        Root<DBUser> from = query.from(DBUser.class);

        query.select(from).where(builder.equal(from.get("id"), id));

        DBUser user = null;
        try {
            user = this.entityManager.createQuery(query).getSingleResult();
        } catch (Exception ignored) {
        }

        this.entityManager.remove(user);
        this.entityManager.flush();
        return user;
    }
}
