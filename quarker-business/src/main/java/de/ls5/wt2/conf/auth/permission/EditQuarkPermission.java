package de.ls5.wt2.conf.auth.permission;

import de.ls5.wt2.entity.DBQuark;
import de.ls5.wt2.entity.DBUser_;
import org.apache.shiro.authz.Permission;

public class EditQuarkPermission implements Permission {

    private final DBQuark quark;

    public EditQuarkPermission(final DBQuark quark) {
        this.quark = quark;
    }

    @Override
    public boolean implies(Permission p) {
        return false;
    }

    public boolean check() {
        return this.quark.getUser().getUsername().equals(DBUser_.USERNAME);
    }
}
