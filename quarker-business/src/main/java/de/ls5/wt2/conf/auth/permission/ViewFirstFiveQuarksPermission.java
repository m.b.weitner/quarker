package de.ls5.wt2.conf.auth.permission;

import java.util.List;

import de.ls5.wt2.entity.DBQuark;
import org.apache.shiro.authz.Permission;

public class ViewFirstFiveQuarksPermission implements Permission {

    private final List<DBQuark> news;

    public ViewFirstFiveQuarksPermission(final List<DBQuark> news) {
        this.news = news;
    }

    @Override
    public boolean implies(Permission p) {
        return false;
    }

    public boolean check() {
        return this.news.size() < 5;
    }
}
