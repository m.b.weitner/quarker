package de.ls5.wt2.conf.auth.permission;

import org.apache.shiro.authz.Permission;

public class ReadQuarkPermission implements Permission {

    @Override
    public boolean implies(Permission p) {
        if (p instanceof EditQuarkPermission) {
            final EditQuarkPermission eqp = (EditQuarkPermission) p;
            return eqp.check();
        }

        return false;
    }
}
